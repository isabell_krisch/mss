.. include:: global.rst.inc


Mission Support System's Documentation
========================================================

.. include:: ../README


.. toctree::
   :maxdepth: 2

   dependencies
   installation
   usage
   deployment
   development
   authors


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

